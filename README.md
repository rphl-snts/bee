# Bee
### Chat notifier for petlove ruby apps

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'bee', github: 'petlove/bee'
```

And then execute:

    $ bundle install

## Usage

For to usage, include into your app:

```ruby
# Rocketchat service
Bee.configure do |config|
  config.plug(type: :rocketchat,
              condition: true,
              name: 'service',
              url: ENV['ROCKETCHAT_API_URL'],
              channel_tokens: {
                failures: ENV['ROCKETCHAT_FAILURES_KEY'],
                default: ENV['ROCKETCHAT_DEFAULT_KEY']
              })
end
```

And in your code type this to notify:

```ruby
Bee.notify!(:failures, text: 'Hello World!')
```