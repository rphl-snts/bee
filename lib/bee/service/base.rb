# frozen_string_literal: true

module Bee
  module Service
    class Invalid < StandardError; end

    class Base
      attr_accessor :name, :url

      def initialize(options)
        options&.each { |k, v| instance_variable_set("@#{k}", v) }

        raise Invalid, 'Name and url are required fields' unless @name && @url
      end

      def notify!(_key, _payload)
        raise NotImplementedError
      end
    end
  end
end
