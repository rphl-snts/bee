# frozen_string_literal: true

require 'rest-client'
require 'json'

module Bee
  module Service
    class UnknowChannelError < StandardError; end

    class RocketChat < Base
      attr_accessor :channel_tokens

      def initialize(options)
        super
        raise Invalid, 'Channel tokens are required fields' unless @channel_tokens
      end

      def notify!(key, payload)
        ::RestClient::Request.execute(
          method: :post,
          url: "#{url}#{token(key)}",
          payload: payload.to_json,
          headers: { 'Content-Type' => 'application/json' }
        )
      end

      private

      def token(key)
        (channel_tokens[key] || channel_tokens[:default]).tap { |token| raise UnknowChannelError unless token }
      end
    end
  end
end
