# frozen_string_literal: true

require 'singleton'

module Bee
  class Configuration
    include Singleton

    attr_accessor :services

    def initialize
      clear!
    end

    def plug(service_options)
      service = build_service(service_options)
      return unless service

      @services << service
    end

    def clear!
      @services = []
    end

    private

    def build_service(service_options)
      return unless service_options&.delete(:condition)

      case service_options.delete(:type)&.to_sym
      when :rocketchat then Bee::Service::RocketChat.new(service_options)
      end
    end
  end
end
