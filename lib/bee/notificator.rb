# frozen_string_literal: true

require 'singleton'

module Bee
  class Notificator
    include Singleton

    def notify!(key, payload)
      Configuration.instance.services.each { |service| service.notify!(key, payload) }
    end
  end
end
