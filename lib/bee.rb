# frozen_string_literal: true

require 'bee/version'
require 'bee/configuration'
require 'bee/notificator'
require 'bee/service/base'
require 'bee/service/rocket_chat'
require 'dotenv/load'

module Bee
  class << self
    def notify!(key, payload)
      Notificator.instance.notify!(key, payload)
    end

    def configure
      yield(Configuration.instance)
    end
  end
end
