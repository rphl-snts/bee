# frozen_string_literal: true

RSpec.describe Bee::Configuration do
  let(:instance) { Bee::Configuration.instance }

  it 'should be a singleton' do
    expect(instance).not_to be_nil
  end

  context '.plug' do
    context 'with nil options' do
      let(:options) { nil }

      it 'shouldnt plug service' do
        instance.plug(options)
        expect(instance.services.empty?).to be_truthy
      end
    end

    context 'with invalid condition' do
      let(:options_false) { { condition: false } }
      let(:options_nil) { {} }

      it 'shouldnt plug service' do
        instance.plug(options_false)
        instance.plug(options_nil)
        expect(instance.services.empty?).to be_truthy
      end
    end

    context 'without type' do
      let(:options) { { condition: true } }

      it 'shouldnt plug service' do
        instance.plug(options)
        expect(instance.services.empty?).to be_truthy
      end
    end

    context 'with unkown type' do
      let(:options) { { type: :slack, condition: true } }

      it 'shouldnt plug service' do
        instance.plug(options)
        expect(instance.services.empty?).to be_truthy
      end
    end

    context 'without required fields' do
      let(:options) { { type: :rocketchat, condition: true } }

      before { allow(Bee::Service::RocketChat).to receive(:new).and_raise(Bee::Service::Invalid) }

      it 'should raise error' do
        expect { instance.plug(options) }.to raise_error(Bee::Service::Invalid)
      end
    end

    context 'with correct fields' do
      let(:options) do
        {
          type: :rocketchat,
          condition: true,
          name: 'test',
          url: ENV['ROCKETCHAT_API_URL'],
          channel_tokens: { failures: ENV['ROCKETCHAT_FAILURES_KEY'] }
        }
      end

      it 'should plug service' do
        instance.plug(options)
        expect(instance.services.any?).to be_truthy
      end
    end
  end
end
