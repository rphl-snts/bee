# frozen_string_literal: true

RSpec.describe Bee::Service::Base do
  it 'should raise error when called to notify' do
    expect { described_class.new(name: 'test', url: 'https').notify!(nil, nil) }.to raise_error(NotImplementedError)
  end

  it 'should raise error without required fields' do
    expect { described_class.new({}) }.to raise_error(Bee::Service::Invalid)
  end
end
