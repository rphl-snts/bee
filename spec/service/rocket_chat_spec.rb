# frozen_string_literal: true

RSpec.describe Bee::Service::RocketChat do
  let(:invalid) { described_class.new(name: 'test', url: ENV['ROCKETCHAT_API_URL']) }
  let(:valid_with_default) do
    described_class.new(name: 'rocketchat-prod',
                        url: ENV['ROCKETCHAT_API_URL'],
                        channel_tokens: {
                          failures: ENV['ROCKETCHAT_FAILURES_KEY'],
                          default: ENV['ROCKETCHAT_DEFAULT_KEY']
                        })
  end
  let(:valid) do
    described_class.new(name: 'rocketchat-prod',
                        url: ENV['ROCKETCHAT_API_URL'],
                        channel_tokens: {
                          failures: ENV['ROCKETCHAT_FAILURES_KEY']
                        })
  end

  context '.initialize' do
    it 'should raise error without required fields' do
      expect { invalid }.to raise_error(Bee::Service::Invalid)
    end
  end

  context '.notify!' do
    context 'when found the channel' do
      it 'should be a success', :vcr do
        expect(valid.notify!(:failures, text: 'Hello world').code).to eq(200)
      end
    end

    context 'when not found the channel but there is a default channel' do
      context 'and there is a default channel' do
        it 'should be a success', :vcr do
          expect(valid_with_default.notify!(:another_channel, text: 'Hello world').code).to eq(200)
        end
      end

      context 'and there isnt a default channel' do
        it 'should raise error' do
          expect { valid.notify!(:another_channel, text: 'Hello world') }.to \
            raise_error(Bee::Service::UnknowChannelError)
        end
      end
    end
  end
end
