# frozen_string_literal: true

RSpec.describe Bee::Notificator do
  let(:instance) { Bee::Notificator.instance }
  let!(:configuration_clear) { Bee::Configuration.instance.clear! }

  it 'should be a singleton' do
    expect(instance).not_to be_nil
  end

  context '.notify' do
    context 'with zero configuration' do
      it 'shouldnt call to notify' do
        expect_any_instance_of(Bee::Service::Base).not_to receive(:notify!)
        instance.notify!(:failures, text: 'Hello world')
      end
    end

    context 'with configurations' do
      let(:prod) do
        {
          type: :rocketchat,
          condition: true,
          name: 'prod',
          url: ENV['ROCKETCHAT_API_URL'],
          channel_tokens: { failures: ENV['ROCKETCHAT_FAILURES_KEY'] }
        }
      end

      let(:test) do
        {
          type: :rocketchat,
          condition: true,
          name: 'test',
          url: ENV['ROCKETCHAT_API_URL'],
          channel_tokens: { failures: ENV['ROCKETCHAT_FAILURES_KEY'] }
        }
      end

      before do
        Bee::Configuration.instance.services = []
        Bee.configure do |config|
          config.plug(prod)
          config.plug(test)
        end
      end

      it 'should call twice to notify' do
        expect(Bee::Configuration.instance.services.length).to eq(2)
        expect(Bee::Configuration.instance.services.first).to receive(:notify!).once
        expect(Bee::Configuration.instance.services.last).to receive(:notify!).once
        instance.notify!(:failures, text: 'Hello world')
      end
    end
  end
end
