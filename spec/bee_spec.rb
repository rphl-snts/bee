# frozen_string_literal: true

RSpec.describe Bee do
  let(:service_options) do
    {
      type: :rocketchat,
      condition: true,
      name: 'test',
      url: ENV['ROCKETCHAT_API_URL'],
      channel_tokens: { failures: ENV['ROCKETCHAT_FAILURES_KEY'] }
    }
  end

  it 'should have a version number' do
    expect(described_class::VERSION).not_to be_nil
  end

  context '#configure' do
    it 'should call to plug configuration' do
      expect(Bee::Configuration.instance).to receive(:plug).once
      described_class.configure { |config| config.plug(service_options) }
    end
  end

  context '#notify' do
    it 'should call notificatior to notify the message' do
      expect(Bee::Notificator.instance).to receive(:notify!).once
      described_class.notify!(:failures, text: 'Hello world')
    end
  end
end
